﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public float speed;

    [SerializeField]
    private ParticleSystem particleSystem;

    [SerializeField]
    private TrailRenderer trailRenderer;

    private CharacterController cc;

    private bool movementStopped;

    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<CharacterController>();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        //main.startColor = new ParticleSystem.MinMaxGradient(new Color(0.5f + moveHorizontal / 2, 0.5f + moveVertical / 2, 0, 1f));
        //particleSystem.startColor = new Color(0.5f + moveHorizontal / 2, 0.5f + moveVertical / 2, 0, 1f);
        Color newColor = new Color(0.5f + (moveHorizontal) / 2, 0.5f + (moveVertical) / 2 + 0.01f, 0, 1f);

        float angle = Mathf.Atan2(moveHorizontal, moveVertical);
        Debug.Log("Angle: " + angle);

        var main = particleSystem.main;
        main.startColor = new ParticleSystem.MinMaxGradient(newColor);

        trailRenderer.startColor = newColor;
        newColor.a = 0f;
        trailRenderer.endColor = newColor;

        /*var colorOverLifetime = particleSystem.colorOverLifetime;

        Gradient ourGradient = new Gradient();
        ourGradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(newColor, 0.0f), new GradientColorKey(newColor, 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(0.0f, 1.0f) }
            );
        colorOverLifetime.color = ourGradient;*/

        Debug.Log("Color " + main.startColor.color);

        if (movement != Vector3.zero)
        {
            //if (movementStopped)
            //{
            //    movementStopped = false;
            //    particleSystem.Play();
            //}

            Vector3 position = transform.position;
            position += movement * speed * Time.deltaTime;
            transform.position = position;
        }
        else
        {
            //particleSystem.Stop();
            movementStopped = true;
        }

        if (transform.position.y < -5f)
        {
            transform.position = new Vector3(0, 0, 0);
        }
    }
}
