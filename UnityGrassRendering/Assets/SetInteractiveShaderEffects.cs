﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetInteractiveShaderEffects : MonoBehaviour
{
    [SerializeField]
    RenderTexture rt;
    [SerializeField]
    Transform target;
    [SerializeField]
    Camera camera;
    // Start is called before the first frame update
    void Awake()
    {
        //rt.format = RenderTextureFormat.ARGBHalf;
        //Shader.SetGlobalTexture("_InteractionRT", rt);
        Shader.SetGlobalFloat("_InteractionCameraOrthographicSize", camera.orthographicSize);
    }

    private void Update()
    {
        //transform.position = new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z);
        Shader.SetGlobalVector("_InteractionCameraPosition", transform.position);
    }


}