#pragma once
#define NOMINMAX // Disable Windows min() max() macros

#include "ContextWrapper.h"
#include "Camera.h"
#include "Mouse.h"
#include <directxcolors.h>
#include <vector>
#include <windows.h>
#include <memory>

namespace Util {
    DirectX::XMVECTORF32 srgbToLinear(const DirectX::XMVECTORF32& color);
    DirectX::XMFLOAT4 srgbToLinearVec(const DirectX::XMVECTORF32& color);
}

class BaseRenderer {
public:
    BaseRenderer();
    ~BaseRenderer() = default;
    int run(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow);

    const float SCREEN_DEPTH = 100.0f;
    const float SCREEN_NEAR = 0.01f;

protected:
    bool shouldExit_ = false;
    std::unique_ptr<DirectX::Mouse> mouse_;
    ContextWrapper context_;

    virtual ContextSettings getSettings() const = 0;
    virtual HRESULT setup() = 0;
    virtual void render() = 0;
    virtual DirectX::Mouse::Mode getInitialMouseMode() = 0;

    Camera camera_;
    DirectX::XMMATRIX projection_;

    float deltaTime_ = 0.0f;
    float timeFromStart = 0.0f;
    static constexpr float smaPeriod_ = 30;
    float deltaTimeSMA_ = 0.0f;
    size_t frameCount_ = 0;

    HRESULT reloadShaders();
    virtual bool reloadShadersInternal() { return true; }
    virtual void handleInput();

    static DirectX::XMMATRIX computeNormalMatrix(const DirectX::XMMATRIX& model);
    static DirectX::XMMATRIX computeNormalMatrix(const std::vector<DirectX::XMMATRIX>& matrices);

    void clearViews() const;

    void showMouse();
};
