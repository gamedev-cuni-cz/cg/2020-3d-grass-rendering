#pragma once
#include "BaseRenderer.h"
#include "ShaderProgram.h"
#include "TexturedCube.h"
#include "Texture.h"
#include "AnisotropicSampler.h"
#include "Plane.h"
#include "Text.h"
#include "Model.h"
#include "PhongLights.h"
#include "LightClass.h"
#include "ShadowShaderClass.h"
#include "RenderTextureClass.h"
#include "DepthShaderClass.h"
#include "ShadowSampler.h"
#include "ColorCube.h"

#include "imgui/imgui.h"
#include "imgui/imgui_impl_win32.h"
#include "imgui/imgui_impl_dx11.h"

namespace GrassRendering {

	// Constant buffer used when drawing objects that use specular shader
	struct SpecularCB {
		DirectX::XMMATRIX World;
		DirectX::XMMATRIX View;
		DirectX::XMMATRIX Projection;
		DirectX::XMMATRIX NormalMatrix;
		DirLight SunLight;
		DirectX::XMFLOAT3 ViewPos;
		int UseSpecular;
	};

	struct GrassCB {
		DirectX::XMMATRIX worldMatrix;
		DirectX::XMMATRIX inverseWorldMatrix;
		DirectX::XMMATRIX viewMatrix;
		DirectX::XMMATRIX projectionMatrix;
		DirectX::XMMATRIX normalMatrix;
		DirectX::XMMATRIX lightViewMatrix;
		DirectX::XMMATRIX lightProjectionMatrix;
		
		DirectX::XMFLOAT4 topColor;
		DirectX::XMFLOAT4 bottomColor;
		DirectX::XMFLOAT4 tilingOffset;
		DirectX::XMFLOAT4 dynamicLightDirection;
		float dynamicLightStrength;
		DirectX::XMFLOAT3 dynamicLightColor;
		DirectX::XMFLOAT2 windFrequency;
		float bendRotationRandom;
		float bladeWidth;
		DirectX::XMFLOAT3 cameraPosition;
		float translucentGain;
		DirectX::XMFLOAT3 dynamicLightPosition;
		float ambientLightStrength;
		DirectX::XMFLOAT3 ambientLightColor;
		
		float bladeWidthRandom;
		float bladeHeight;
		float bladeHeightRandom;
		float tessellationUniform;
		float MinTessellationDistance;
		float MaxTessellationDistance;
		float edgesPerScreenHeight;
		float windStrength;
		float bladeForward;
		float bladeCurve;
		float time;

		int MaxTessellationFactor;
		int loddingMethod;
		int backfaceCulling;
		
		int bladesLodding;
		float minTessellationDistance;
		float maxTessellationDistance;
	};

	struct ConstantBuffer {
		DirectX::XMMATRIX World;
		DirectX::XMMATRIX View;
		DirectX::XMMATRIX Projection;
		DirectX::XMMATRIX NormalMatrix;
	};

	struct SolidCB {
		DirectX::XMMATRIX World;
		DirectX::XMMATRIX View;
		DirectX::XMMATRIX Projection;
		DirectX::XMFLOAT4 vOutputColor;
	};

	struct ShadowConstBuffer {
		DirectX::XMMATRIX World;
		DirectX::XMMATRIX View;
		DirectX::XMMATRIX Projection;
	};

	struct ShadowDisplayBuffer {
		DirectX::XMMATRIX World;
	};

	struct PhongShadowsCB {
		DirectX::XMMATRIX World;
		DirectX::XMMATRIX View;
		DirectX::XMMATRIX Projection;
		DirectX::XMMATRIX NormalMatrix;
		DirectX::XMMATRIX LightView;
		DirectX::XMMATRIX LightProjection;
		DirLight SunLight;
		DirectX::XMFLOAT3 ViewPos;
	};

	class GrassSettingsWrapper {
	public:
		ImVec4 top_color = ImVec4(0.5792569f, 0.846f, 0.3297231f, 1.00f);
		ImVec4 bottom_color = ImVec4(0.06129726f, 0.378f, 0.07151345f, 1.00f);
		ImVec2 wind_frequency = ImVec2(0.05f, 0.05f);
		ImVec2 tiling_offset = ImVec2(0.01f, 0.01f);
		float bend_rotation_random = 0.2;
		float blade_width = 0.05f;
		float blade_width_random = 0.02f;
		float blade_height = 0.7f;
		float blade_height_random = 0.3f;
		float tessellation_uniform = 15.0f;
		float wind_strength = 0.2f;
		float blade_forward = 0.38f;
		float blade_curve = 2.0f;
		float pixels_per_edge = 40.0f;
		float min_tesselation_distance = 30.0f;
		float max_tesselation_distance = 50.0f;
		int max_tesselation_factor = 32;
		ImVec4 ambient_light_color = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
		float ambient_light_strength = 0.2f;
		float translucent_gain = 0.5f;
		ImVec4 dynamic_light_color = ImVec4(0.3725f, 0.3607f, 0.3019f, 1.0f);
		float dynamic_light_strenght = 1.5f;
		bool backface_culling = false;
		int lodding_method = 2;
		bool blades_lodding = false;
		float min_lodding_distance = 10.0f;
		float max_lodding_distance = 12.0f;

		void setDefaults() {
			top_color = ImVec4(0.5792569f, 0.846f, 0.3297231f, 1.00f);
			bottom_color = ImVec4(0.06129726f, 0.378f, 0.07151345f, 1.00f);
			wind_frequency = ImVec2(0.05f, 0.05f);
			tiling_offset = ImVec2(0.01f, 0.01f);
			bend_rotation_random = 0.2;
			blade_width = 0.05f;
			blade_width_random = 0.02f;
			blade_height = 0.7f;
			blade_height_random = 0.3f;
			tessellation_uniform = 15.0f;
			wind_strength = 0.2f;
			blade_forward = 0.38f;
			blade_curve = 2.0f;
			pixels_per_edge = 40.0f;
			min_tesselation_distance = 30.0f;
			max_tesselation_distance = 50.0f;
			max_tesselation_factor = 32;
			ambient_light_color = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
			ambient_light_strength = 0.2f;
			translucent_gain = 0.5f;
			dynamic_light_color = ImVec4(0.3725f, 0.3607f, 0.3019f, 1.0f);
			dynamic_light_strenght = 1.5f;
			backface_culling = true;
			lodding_method = 2;
			blades_lodding = false;
			min_lodding_distance = 10.0f;
			max_lodding_distance = 12.0f;
		}

		void updateGrassCB(GrassCB& grassCB, ContextWrapper& context) {
			grassCB.dynamicLightColor = DirectX::XMFLOAT3(dynamic_light_color.x, dynamic_light_color.y, dynamic_light_color.z);
			grassCB.dynamicLightStrength = dynamic_light_strenght;
			grassCB.topColor = DirectX::XMFLOAT4(top_color.x, top_color.y, top_color.z, top_color.w);
			grassCB.bottomColor = DirectX::XMFLOAT4(bottom_color.x, bottom_color.y, bottom_color.z, bottom_color.w);
			grassCB.tilingOffset = DirectX::XMFLOAT4(tiling_offset.x, tiling_offset.y, 0, 0);
			grassCB.windFrequency = DirectX::XMFLOAT2(wind_frequency.x, wind_frequency.y);
			grassCB.bendRotationRandom = bend_rotation_random;
			grassCB.bladeWidth = blade_width;
			grassCB.bladeWidthRandom = blade_width_random;
			grassCB.bladeHeight = blade_height;
			grassCB.bladeHeightRandom = blade_height_random;
			grassCB.tessellationUniform = tessellation_uniform;
			grassCB.windStrength = wind_strength;
			grassCB.bladeForward = blade_forward;
			grassCB.bladeCurve = blade_curve;
			grassCB.edgesPerScreenHeight = context.HEIGHT / pixels_per_edge;
			grassCB.MinTessellationDistance = min_tesselation_distance;
			grassCB.MaxTessellationDistance = max_tesselation_distance;
			grassCB.MaxTessellationFactor = max_tesselation_factor;
			grassCB.loddingMethod = lodding_method;
			grassCB.backfaceCulling = backface_culling;
			grassCB.translucentGain = translucent_gain;
			grassCB.ambientLightColor = DirectX::XMFLOAT3(ambient_light_color.x, ambient_light_color.y, ambient_light_color.z);
			grassCB.ambientLightStrength = ambient_light_strength;
			grassCB.bladesLodding = blades_lodding;
			grassCB.minTessellationDistance = min_lodding_distance;
			grassCB.maxTessellationDistance = min_lodding_distance;
		}
	};

	class GrassRenderer : public BaseRenderer {
	public:
		GrassRenderer();
		~GrassRenderer();

		// A method for displaying tooltips in the ui
		static void HelpMarker(const char* desc)
		{
			ImGui::TextDisabled("(?)");
			if (ImGui::IsItemHovered())
			{
				ImGui::BeginTooltip();
				ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
				ImGui::TextUnformatted(desc);
				ImGui::PopTextWrapPos();
				ImGui::EndTooltip();
			}
		}

	protected:
		using TextureShader = ShaderProgram<PhongShadowsCB>;
		using GrassShader = ShaderProgram<GrassCB>;
		using GrassShadowsShader = ShaderProgram<GrassCB>;
		using GroundShader = ShaderProgram<SpecularCB>;
		using SolidShader = ShaderProgram<SolidCB>;
		using GeomShader = ShaderProgram<ConstantBuffer>;
		using ShadowShader = ShaderProgram<ShadowConstBuffer>;
		using ShadowDisplayShader = ShaderProgram<ShadowDisplayBuffer>;

		bool drawFromLightView_ = false;

		std::chrono::system_clock::time_point startTime;

		//using SolidShader = ShaderProgram<SpecularCB>;
		std::unique_ptr<GeomShader> normalShader_;
		std::unique_ptr<GrassShader> grassShader_;
		std::unique_ptr<GrassShadowsShader> grassShadowsShader_;
		std::unique_ptr<SolidShader> solidShader_;

		std::unique_ptr<Texture> seaFloorTexture_;
		std::unique_ptr<Texture> windTexture_;

		std::unique_ptr<GroundShader> ground_shader;
		std::unique_ptr<ShadowShader> shadowShader_;
		std::unique_ptr<ShadowDisplayShader> shadowMapDisplayShader_;
		std::unique_ptr<TextureShader> texturedPhong_;
		
		std::unique_ptr<Text::Text> frameTimeText_;		
		
		std::unique_ptr<PointWrapSampler> pointSampler_;
		std::unique_ptr<AnisotropicSampler> diffuseSampler_;
		std::unique_ptr<ShadowSampler> shadowSampler_;
				
		std::unique_ptr<ColorCube> cubeModel_;
		std::unique_ptr<Models::Model> planeModel_;
		std::unique_ptr<Models::Model> planetModel_;
		std::unique_ptr<Quad> shadowMapDisplay_;
		std::unique_ptr<Plane> plane_;

		std::unique_ptr < GrassRendering::GrassSettingsWrapper> planeSettingsWrapper;
		std::unique_ptr < GrassRendering::GrassSettingsWrapper> planetSettingsWrapper;
		
		GrassCB planeGrassCB;
		GrassCB planetGrassCB;

		// Imgui stuff
		int current_content = 0;
		int current_settings = 0;
		bool update;
		bool renderPlanet = false;
		bool renderPlane = true;
		bool renderCube = true;
		bool shadows = true;
		std::string selected_template = "default";

		// =======
		// Shadows
		// =======

		// Resolution of the shadowmap
		static constexpr UINT SHADOW_MAP_WIDTH = 1920;
		static constexpr UINT SHADOW_MAP_HEIGHT = 1920;

		ID3D11DepthStencilView* shadowMapDepthView_ = nullptr;
		ID3D11ShaderResourceView* shadowShaderResourceView_ = nullptr;
		D3D11_VIEWPORT shadowViewPort_;
		
		ID3D11Buffer* vertexBuffer_ = nullptr;

		DirectX::Mouse::Mode getInitialMouseMode() override;
		HRESULT setup() override;
		bool reloadShadersInternal() override;
		void handleInput() override;
		void render() override;
		ContextSettings getSettings() const override;
		bool renderModelImGUI(std::unique_ptr<GrassSettingsWrapper> & settingsWrapper);
		bool renderImGUI();

		void getSettingsPreset(std::string name, std::unique_ptr<GrassSettingsWrapper>& settingsWrapper);
	};
}
