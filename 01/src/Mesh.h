#pragma once

#include <d3d11.h>
#include <vector>
#include <DirectXMath.h>
#include <string>
#include <iostream>
#include "ModelTexture.h"

namespace Models {

struct Vertex {
    DirectX::XMFLOAT3 Position;
    DirectX::XMFLOAT3 Normal;
    DirectX::XMFLOAT2 TexCoords;
    DirectX::XMFLOAT3 Tangent;
    //DirectX::XMFLOAT3 Bitangent;
};

class Mesh {
private:
    ID3D11Buffer* vertexBuffer_;
    ID3D11Buffer* indexBuffer_;
    UINT indexCount_;
    std::vector<ModelTexture> textures_;

public:
    Mesh(
        ID3D11Device* device,
        const std::vector<Vertex>& vertices,
        const std::vector<unsigned int>& indices,
        std::vector<ModelTexture>&& textures
    ) 
            : textures_(std::move(textures)) {
        D3D11_BUFFER_DESC bd;
        ZeroMemory(&bd, sizeof(bd));
        bd.Usage = D3D11_USAGE_DEFAULT;
        bd.ByteWidth = static_cast<UINT>(sizeof(Vertex) * vertices.size());
        bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
        bd.CPUAccessFlags = 0;
        D3D11_SUBRESOURCE_DATA InitData;
        ZeroMemory(&InitData, sizeof(InitData));
        InitData.pSysMem = vertices.data();
        auto hr = device->CreateBuffer(&bd, &InitData, &vertexBuffer_);
        if (FAILED(hr)) {
            MessageBox(nullptr, L"Failed to create vertex buffer", L"Error", MB_OK);
            return;
        }

        // Create index buffer
        bd.Usage = D3D11_USAGE_DEFAULT;
        bd.ByteWidth = static_cast<UINT>(sizeof(unsigned int) * indices.size());
        bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
        bd.CPUAccessFlags = 0;
        InitData.pSysMem = indices.data();
        hr = device->CreateBuffer(&bd, &InitData, &indexBuffer_);
        if (FAILED(hr)) {
            MessageBox(nullptr, L"Failed to create index buffer", L"Error", MB_OK);
            return;
        }
        indexCount_ = static_cast<UINT>(indices.size());
    }

    ~Mesh() {
        if (vertexBuffer_) vertexBuffer_->Release();
        if (indexBuffer_) indexBuffer_->Release();
    }

    Mesh(const Mesh&) = delete;
    Mesh operator=(const Mesh&) = delete;

    Mesh(Mesh&& other) noexcept {
        vertexBuffer_ = other.vertexBuffer_;
        other.vertexBuffer_ = nullptr;
        indexBuffer_ = other.indexBuffer_;
        other.indexBuffer_ = nullptr;
        indexCount_ = other.indexCount_;
        other.indexCount_ = 0;
        textures_ = std::move(other.textures_);
    }
    Mesh& operator=(Mesh&& other) noexcept {
        vertexBuffer_ = other.vertexBuffer_;
        other.vertexBuffer_ = nullptr;
        indexBuffer_ = other.indexBuffer_;
        other.indexBuffer_ = nullptr;
        indexCount_ = other.indexCount_;
        other.indexCount_ = 0;
        textures_ = std::move(other.textures_);

        return *this;
    }

    void draw(ID3D11DeviceContext* context, D3D10_PRIMITIVE_TOPOLOGY topology = D3D10_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST) const {
        for (UINT i = 0; i < textures_.size(); ++i) {
            if (textures_[i].Type != TextureType::Unknown) {
                textures_[i].Texture->use(context, static_cast<int>(textures_[i].Type));
            } else {
                std::cout << "Trying to use texture of unknown type" << std::endl;
            }
        }

        const UINT stride = sizeof(Vertex);
        const UINT offset = 0;
        context->IASetVertexBuffers(0, 1, &vertexBuffer_, &stride, &offset);
        context->IASetIndexBuffer(indexBuffer_, DXGI_FORMAT_R32_UINT, 0);
        context->IASetPrimitiveTopology(topology);
        context->DrawIndexed(indexCount_, 0, 0);
    }
};

}
