#include "ShaderProgram.h"

bool Shaders::isLastCompileOK = false;

HRESULT Shaders::CompileShaderFromFile(const WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut) {
    HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;

    // Disable optimizations to further improve shader debugging
    dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

    ID3DBlob* pErrorBlob = nullptr;
    std::wcout << "Compiling: " << szFileName << " | " << szEntryPoint << std::endl;
    hr = D3DCompileFromFile(szFileName, nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, szEntryPoint, szShaderModel, dwShaderFlags, 0, ppBlobOut, &pErrorBlob);
    if (FAILED(hr)) {
        if (pErrorBlob) {
            std::cout << "Shader compile errors: " << std::endl;
            std::cout << reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer()) << std::endl;
            OutputDebugStringA(reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer()));
            pErrorBlob->Release();
        }
        Shaders::isLastCompileOK = false;
        return hr;
    }
    if (pErrorBlob) {
        pErrorBlob->Release();
    }

    Shaders::isLastCompileOK = true;
    std::cout << "Success!" << std::endl;
    return S_OK;
}