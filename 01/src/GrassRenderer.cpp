#include "GrassRenderer.h"
#include <directxcolors.h>
#include <iostream>

#include "Layouts.h"
#include "Transform.h"
#include "WinKeyMap.h"


namespace GrassRendering {

using namespace DirectX;

struct PosVertex {
    XMFLOAT4 Position;
};

ID3D11RasterizerState* state = nullptr;

GrassRenderer::GrassRenderer() : BaseRenderer()
{

}

GrassRenderer::~GrassRenderer() {
    if (shadowMapDepthView_) {
        shadowMapDepthView_->Release();
    }

    if (shadowShaderResourceView_) {
        shadowShaderResourceView_->Release();
    }

    // Cleanup
    ImGui_ImplDX11_Shutdown();
    ImGui_ImplWin32_Shutdown();
    ImGui::DestroyContext();
}

HRESULT GrassRenderer::setup() {
    auto hr = BaseRenderer::setup();
    if (FAILED(hr))
        return hr;

    startTime = std::chrono::system_clock::now();

    // Setup the ground
    //std::string path = "models/grass_plane.obj";
    std::string path = "models/plane_detailed_tangents2.fbx";
    //std::string path = "models/ball_tangents.fbx";
    //std::string path = "models/ball.obj";
    //std::string path = "models/curvy_plane/sample_curvy_plane.obj";
    //std::string path = "models/curvy_plane/curvy_plane.obj";
    planeModel_ = std::make_unique<Models::Model>(context_, "models/plane_detailed_tangents2.fbx");
    planetModel_ = std::make_unique<Models::Model>(context_, "models/ball_tangents.fbx");
    cubeModel_ = std::make_unique<ColorCube>(context_.d3dDevice_);
    shadowMapDisplay_ = std::make_unique<Quad>(context_.d3dDevice_);
    plane_ = std::make_unique<Plane>(context_.d3dDevice_);

    hr = reloadShaders();
    if (FAILED(hr)) {
        return hr;
    }
    
    // Textures
    //seaFloorTexture_ = std::make_unique<Texture>(context_.d3dDevice_, context_.immediateContext_, L"textures/seafloor.dds", true);
    seaFloorTexture_ = std::make_unique<Texture>(context_.d3dDevice_, context_.immediateContext_, L"textures/grass1.dds", true);
    //seaFloorTexture_ = std::make_unique<Texture>(context_.d3dDevice_, context_.immediateContext_, L"textures/grass2.dds", true);
    //seaFloorTexture_ = std::make_unique<Texture>(context_.d3dDevice_, context_.immediateContext_, L"textures/grass3.dds", true);
    //seaFloorTexture_ = std::make_unique<Texture>(context_.d3dDevice_, context_.immediateContext_, L"textures/grass4.dds", true);
    //seaFloorTexture_ = std::make_unique<Texture>(context_.d3dDevice_, context_.immediateContext_, L"textures/grass5.dds", true);
    //seaFloorTexture_ = std::make_unique<Texture>(context_.d3dDevice_, context_.immediateContext_, L"textures/grass6.dds", true);
    windTexture_ = std::make_unique<Texture>(context_.d3dDevice_, context_.immediateContext_, L"textures/wind.dds", true);

    frameTimeText_ = std::make_unique<Text::Text>(context_.d3dDevice_, context_.immediateContext_, "Frame time: 0");
        
    // Samplers
    diffuseSampler_ = std::make_unique<AnisotropicSampler>(context_.d3dDevice_);
    shadowSampler_ = std::make_unique<ShadowSampler>(context_.d3dDevice_);
    pointSampler_ = std::make_unique<PointWrapSampler>(context_.d3dDevice_);

    // =======
    // Shadows
    // =======
    // If we want the shadowmap to be different size than our standard viewport
    // we need to create new one and bind it when we generate the shadowmap
    shadowViewPort_.Width = static_cast<FLOAT>(SHADOW_MAP_WIDTH);
    shadowViewPort_.Height = static_cast<FLOAT>(SHADOW_MAP_HEIGHT);
    shadowViewPort_.MinDepth = 0.0f;
    shadowViewPort_.MaxDepth = 1.0f;
    shadowViewPort_.TopLeftX = 0;
    shadowViewPort_.TopLeftY = 0;

    D3D11_TEXTURE2D_DESC texDesc;
    ZeroMemory(&texDesc, sizeof(D3D11_TEXTURE2D_DESC));
    texDesc.Width = SHADOW_MAP_WIDTH;
    texDesc.Height = SHADOW_MAP_HEIGHT;
    texDesc.MipLevels = 1;
    texDesc.ArraySize = 1;
    // We will look at this texture with 2 different views -> typeless
    texDesc.Format = DXGI_FORMAT_R32_TYPELESS;
    texDesc.SampleDesc.Count = 1;
    texDesc.SampleDesc.Quality = 0;
    texDesc.Usage = D3D11_USAGE_DEFAULT;
    texDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
    texDesc.CPUAccessFlags = 0;
    texDesc.MiscFlags = 0;

    ID3D11Texture2D* shadowMap = nullptr;
    hr = context_.d3dDevice_->CreateTexture2D(&texDesc, nullptr, &shadowMap);
    if (FAILED(hr))
        return hr;

    D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
    ZeroMemory(&descDSV, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
    // Put all precision to the depth
    descDSV.Format = DXGI_FORMAT_D32_FLOAT;
    descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    descDSV.Texture2D.MipSlice = 0;

    hr = context_.d3dDevice_->CreateDepthStencilView(shadowMap, &descDSV, &shadowMapDepthView_);
    if (FAILED(hr))
        return hr;

    D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
    ZeroMemory(&srvDesc, sizeof(srvDesc));
    // In shader sample it like regular texture with single red channel
    srvDesc.Format = DXGI_FORMAT_R32_FLOAT;
    srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    srvDesc.Texture2D.MipLevels = texDesc.MipLevels;
    srvDesc.Texture2D.MostDetailedMip = 0;

    hr = context_.d3dDevice_->CreateShaderResourceView(shadowMap, &srvDesc, &shadowShaderResourceView_);
    if (FAILED(hr))
        return hr;

    // The views have their own reference and since we don't need the texture anymore we can just release it
    // (decrease refcount) to avoid leaks on example exit
    shadowMap->Release();

    // ==============================
    // Setup point for grass position
    // ==============================

    std::vector<PosVertex> vertices = {
        { XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) }
    };

    D3D11_BUFFER_DESC bd;
    ZeroMemory(&bd, sizeof(bd));
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(PosVertex);
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bd.CPUAccessFlags = 0;
    D3D11_SUBRESOURCE_DATA InitData;
    ZeroMemory(&InitData, sizeof(InitData));
    InitData.pSysMem = vertices.data();
    hr = context_.d3dDevice_->CreateBuffer(&bd, &InitData, &vertexBuffer_);
    if (FAILED(hr)) {
        MessageBox(nullptr, L"Failed to create vertex buffer", L"Error", MB_OK);
        return hr;
    }

    // ==================
    // Setup face culling
    // ==================
    D3D11_RASTERIZER_DESC CurrentRasterizerState;
    CurrentRasterizerState.FillMode = D3D11_FILL_SOLID;
    CurrentRasterizerState.CullMode = D3D11_CULL_NONE;// D3D11_CULL_FRONT;
    CurrentRasterizerState.FrontCounterClockwise = true;
    CurrentRasterizerState.DepthBias = false;
    CurrentRasterizerState.DepthBiasClamp = 0;
    CurrentRasterizerState.SlopeScaledDepthBias = 0;
    CurrentRasterizerState.DepthClipEnable = true;
    CurrentRasterizerState.ScissorEnable = false;
    CurrentRasterizerState.MultisampleEnable = true;
    CurrentRasterizerState.AntialiasedLineEnable = false;
    
    hr = context_.d3dDevice_->CreateRasterizerState(&CurrentRasterizerState, &state);
    if (FAILED(hr)) {
        MessageBox(nullptr, L"Failed to create rasterizer state", L"Error", MB_OK);
        return hr;
    }
    context_.immediateContext_->RSSetState(state);

    // =====================
    // Enable alpha blending
    // =====================

    D3D11_BLEND_DESC blendDesc;
    ZeroMemory(&blendDesc, sizeof D3D11_BLEND_DESC);
    blendDesc.AlphaToCoverageEnable = true;
    blendDesc.RenderTarget[0].BlendEnable = false;
    blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
    blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
    blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

    ID3D11BlendState* blendState;
    hr = context_.d3dDevice_->CreateBlendState(&blendDesc, &blendState);
    if (FAILED(hr)) {
        MessageBox(nullptr, L"Failed to create blend state", L"Error", MB_OK);
        return hr;
    }

    float bl[] = { 0.0f, 0.0f, 0.0f, 0.0f };
    context_.immediateContext_->OMSetBlendState(blendState, bl, 0xffffffff); 

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();

    // Setup Platform/Renderer bindings
    ImGui_ImplWin32_Init(context_.hWnd_);
    ImGui_ImplDX11_Init(context_.d3dDevice_, context_.immediateContext_);

    planeSettingsWrapper = std::make_unique<GrassSettingsWrapper>(); 
    planetSettingsWrapper = std::make_unique<GrassSettingsWrapper>();

    // Update the grass buffer with defaults before we start
    planeSettingsWrapper->updateGrassCB(planeGrassCB, context_);
    planetSettingsWrapper->updateGrassCB(planetGrassCB, context_);

    return S_OK;
}

bool GrassRenderer::reloadShadersInternal() {
    return
        Shaders::makeShader<GrassShader>(
            grassShader_,
            context_.d3dDevice_,
            L"shaders/Grass.fx", "VS",
            L"shaders/Grass.fx", "PS",
            Layouts::POS_NORM_TANGENT_LAYOUT,
            L"shaders/Grass.fx", "hull",
            L"shaders/Grass.fx", "domain",
            L"shaders/Grass.fx", "GS"
            )
        && Shaders::makeShader<GrassShadowsShader>(
            grassShadowsShader_,
            context_.d3dDevice_,
            L"shaders/Grass.fx", "VS",
            L"shaders/GrassShadows.fx", "PS",
            Layouts::POS_NORM_TANGENT_LAYOUT,
            L"shaders/Grass.fx", "hull",
            L"shaders/Grass.fx", "domain",
            L"shaders/Grass.fx", "GS"
            )
        && Shaders::makeShader<GroundShader>(
            ground_shader,
            context_.d3dDevice_,
            L"shaders/ModelPhong.fx", "VS",
            L"shaders/ModelPhong.fx", "PS",
            Layouts::POS_NORM_UV_LAYOUT)
        && Shaders::makeShader<GeomShader>(
            normalShader_,
            context_.d3dDevice_,
            L"shaders/NormalGeomShader.fx", "VS",
            L"shaders/NormalGeomShader.fx", "PS",
            Layouts::POS_NORM_TANGENT_LAYOUT,
            nullptr, nullptr, 
            nullptr, nullptr,
            L"shaders/NormalGeomShader.fx", "GS") 
        && Shaders::makeShader<TextureShader>(
            texturedPhong_,
            context_.d3dDevice_,
            L"shaders/PhongShadows.fx", "VS",
            L"shaders/PhongShadows.fx", "PS",
            Layouts::TEXTURED_LAYOUT)
        && Shaders::makeShader<SolidShader>(
            solidShader_,
            context_.d3dDevice_,
            L"shaders/Solid.fx", "VS",
            L"shaders/Solid.fx", "PSSolid",
            Layouts::POS_NORM_COL_LAYOUT)
        && Shaders::makeShader<ShadowShader>(
            shadowShader_,
            context_.d3dDevice_,
            L"shaders/Shadows.fx", "VS_Shadow",
            L"shaders/Shadows.fx", "PS_Shadow",
            Layouts::TEXTURED_LAYOUT)
        && Shaders::makeShader<ShadowDisplayShader>(
            shadowMapDisplayShader_,
            context_.d3dDevice_,
            L"shaders/ShadowMapQuadShader.fx", "VS",
            L"shaders/ShadowMapQuadShader.fx", "PS",
            Layouts::POS_UV_LAYOUT)
        ;
   }

void GrassRenderer::handleInput() {
    BaseRenderer::handleInput();

    if (GetAsyncKeyState(WinKeyMap::Q) & 1) {
        drawFromLightView_ = !drawFromLightView_;
    }
}       

void GrassRenderer::render() {   
    BaseRenderer::render();

    const XMFLOAT3 sunPos = XMFLOAT3(-20.0f, 20.0f, -20.0f);
    const auto focus = XMFLOAT3(0, 0, 0);
    const auto up = XMFLOAT3(0, 1, 0);

    const XMMATRIX lightProjection = XMMatrixOrthographicLH(30.0f, 20.0f, 1.0f, 100.0f);
    const XMMATRIX lightView = XMMatrixLookAtLH(XMLoadFloat3(&sunPos), XMLoadFloat3(&focus), XMLoadFloat3(&up));
    const Transform planeTransform = Transform(XMFLOAT3(0, 0, 0), XMFLOAT3(), XMFLOAT3(1.0f, 1.0f, 1.0f));
    const Transform planetTransform = Transform(XMFLOAT3(-10, 10, 0), XMFLOAT3(), XMFLOAT3(2.0f, 2.0f, 2.0f));
    const Transform coloredCubeTransform(XMFLOAT3(3.0f, 2.0, 0.0f), XMFLOAT3(), XMFLOAT3(1.0f, 1.0f, 1.0f));
    const Transform sunCubeTransform(sunPos, XMFLOAT3(), XMFLOAT3(0.10f, 0.10f, 0.10f));

    frameTimeText_->setText("Frame time (ms): " + std::to_string(deltaTimeSMA_ * 1000));
    auto now = std::chrono::system_clock::now();
    std::chrono::duration<float> elapsedTime = now - startTime;

    // Assign projection and view matrix according to the mode of drawing
    DirectX::XMMATRIX projectionMatrix, viewMatrix;
    if (drawFromLightView_) {
        projectionMatrix = XMMatrixTranspose(lightProjection);
        viewMatrix = XMMatrixTranspose(lightView);
    }
    else {
        projectionMatrix = XMMatrixTranspose(projection_);
        viewMatrix = XMMatrixTranspose(camera_.getViewMatrix());
    }

    // Initialize planet grass cb
    planetGrassCB.cameraPosition = camera_.Position;
    planetGrassCB.dynamicLightDirection = XMFLOAT4(-sunPos.x, -sunPos.y, -sunPos.z, 1.0f);
    planetGrassCB.dynamicLightPosition = sunPos;
    planetGrassCB.time = elapsedTime.count();
    planetGrassCB.lightViewMatrix = XMMatrixTranspose(lightView);
    planetGrassCB.lightProjectionMatrix = XMMatrixTranspose(lightProjection);

    // Create a grass constant buffer
    // Initialize plane grass cb
    planeGrassCB.cameraPosition = camera_.Position;
    planeGrassCB.dynamicLightDirection = XMFLOAT4(-sunPos.x, -sunPos.y, -sunPos.z, 1.0f);
    planeGrassCB.dynamicLightPosition = sunPos;
    planeGrassCB.time = elapsedTime.count();
    planeGrassCB.lightViewMatrix = XMMatrixTranspose(lightView);
    planeGrassCB.lightProjectionMatrix = XMMatrixTranspose(lightProjection);

    // Prepare the scene for drawing shadows
    context_.immediateContext_->OMSetRenderTargets(0, nullptr, shadowMapDepthView_);
    context_.immediateContext_->ClearDepthStencilView(shadowMapDepthView_, D3D11_CLEAR_DEPTH, 1.0f, 0);
    context_.immediateContext_->RSSetViewports(1, &shadowViewPort_);

    // ==================
    // Generate shadowmap
    // ==================
    if (shadows)
    {
        // Draw cube
        ShadowConstBuffer cb;
        cb.Projection = XMMatrixTranspose(lightProjection);
        cb.View = XMMatrixTranspose(lightView);

        if (renderCube) {
            shadowShader_->use(context_.immediateContext_);
            cb.World = XMMatrixTranspose(coloredCubeTransform.generateModelMatrix());
            shadowShader_->updateConstantBuffer(context_.immediateContext_, cb);
            cubeModel_->draw(context_.immediateContext_);
        }

        // Render the grass shadows
        if (renderPlane) {
            planeGrassCB.viewMatrix = XMMatrixTranspose(lightView);
            planeGrassCB.projectionMatrix = XMMatrixTranspose(lightProjection);
            cb.World = XMMatrixTranspose(planeTransform.generateModelMatrix());
            shadowShader_->use(context_.immediateContext_);
            shadowShader_->updateConstantBuffer(context_.immediateContext_, cb);
            planeModel_->draw(context_.immediateContext_);

            planeGrassCB.worldMatrix = XMMatrixTranspose(planeTransform.generateModelMatrix());
            planeGrassCB.inverseWorldMatrix = XMMatrixInverse(nullptr, planeGrassCB.worldMatrix);
            planeGrassCB.normalMatrix = computeNormalMatrix(planeGrassCB.worldMatrix);
            diffuseSampler_->use(context_.immediateContext_, 0);
            windTexture_->use(context_.immediateContext_, 0);
            diffuseSampler_->useGS(context_.immediateContext_, 0);
            windTexture_->useGS(context_.immediateContext_, 0);
            grassShadowsShader_->updateConstantBuffer(context_.immediateContext_, planeGrassCB);
            grassShadowsShader_->use(context_.immediateContext_);
            planeModel_->draw(context_.immediateContext_, D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
        }

        if (renderPlanet) {
            planetGrassCB.viewMatrix = XMMatrixTranspose(lightView);
            planetGrassCB.projectionMatrix = XMMatrixTranspose(lightProjection);
            cb.World = XMMatrixTranspose(planetTransform.generateModelMatrix());
            shadowShader_->updateConstantBuffer(context_.immediateContext_, cb);
            planetModel_->draw(context_.immediateContext_);

            planetGrassCB.worldMatrix = XMMatrixTranspose(planetTransform.generateModelMatrix());
            planetGrassCB.inverseWorldMatrix = XMMatrixInverse(nullptr, planetGrassCB.worldMatrix);
            planetGrassCB.normalMatrix = computeNormalMatrix(planetGrassCB.worldMatrix);
            diffuseSampler_->use(context_.immediateContext_, 0);
            windTexture_->use(context_.immediateContext_, 0);
            diffuseSampler_->useGS(context_.immediateContext_, 0);
            windTexture_->useGS(context_.immediateContext_, 0);
            grassShadowsShader_->updateConstantBuffer(context_.immediateContext_, planetGrassCB);
            grassShadowsShader_->use(context_.immediateContext_);
            planetModel_->draw(context_.immediateContext_, D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
        }
    }

    // todo: Separate method
    // ==============
    // Draw the scene
    // ==============
    {
        context_.immediateContext_->OMSetRenderTargets(1, &context_.renderTargetView_, context_.depthStencilView_);
        context_.immediateContext_->ClearRenderTargetView(context_.renderTargetView_, Util::srgbToLinear(DirectX::Colors::MidnightBlue));
        context_.immediateContext_->ClearDepthStencilView(context_.depthStencilView_, D3D11_CLEAR_DEPTH, 1.0f, 0);
        context_.immediateContext_->RSSetViewports(1, &context_.viewPort_);

        // Start the Dear ImGui frame
        ImGui_ImplDX11_NewFrame();
        ImGui_ImplWin32_NewFrame();
        ImGui::NewFrame();

        // ===========================
        // Draw the shadow map display
        // ===========================
        if (shadows) {
            const float mapDisplaySize = 0.2f;
            Transform shadowMapDisplayTransform(
                XMFLOAT3(1 - mapDisplaySize, -1 + mapDisplaySize * context_.getAspectRatio(), 0),
                XMFLOAT3(0, 0, 0),
                XMFLOAT3(mapDisplaySize, mapDisplaySize * context_.getAspectRatio(), mapDisplaySize)
            );

            ShadowDisplayBuffer sdcb;
            sdcb.World = XMMatrixTranspose(shadowMapDisplayTransform.generateModelMatrix());

            shadowMapDisplayShader_->use(context_.immediateContext_);
            shadowMapDisplayShader_->updateConstantBuffer(context_.immediateContext_, sdcb);
            context_.immediateContext_->PSSetShaderResources(0, 1, &shadowShaderResourceView_);
            pointSampler_->use(context_.immediateContext_, 0);
            shadowMapDisplay_->draw(context_.immediateContext_);
        }
        /////////////////////////////

        // todo: Put to custom method to render objects
        if (renderPlanet) {
            PhongShadowsCB phongCB;
            phongCB.Projection = projectionMatrix;
            phongCB.View = viewMatrix;
            phongCB.World = XMMatrixTranspose(planetTransform.generateModelMatrix());
            phongCB.LightView = XMMatrixTranspose(lightView);
            phongCB.LightProjection = XMMatrixTranspose(lightProjection);
            phongCB.SunLight.Color = SUN_YELLOW;
            phongCB.SunLight.Direction = XMFLOAT4(-sunPos.x, -sunPos.y, -sunPos.z, 1.0f);
            phongCB.NormalMatrix = computeNormalMatrix(phongCB.World);

            // todo: textures on the ground do not work
            diffuseSampler_->use(context_.immediateContext_, 0);
            shadowSampler_->use(context_.immediateContext_, 1);
            context_.immediateContext_->PSSetShaderResources(1, 1, &shadowShaderResourceView_);
            seaFloorTexture_->use(context_.immediateContext_, 0);
            texturedPhong_->use(context_.immediateContext_);
            texturedPhong_->updateConstantBuffer(context_.immediateContext_, phongCB);
            planetModel_->draw(context_.immediateContext_);
        }

        if (renderPlane) {
            PhongShadowsCB phongCB;
            phongCB.Projection = projectionMatrix;
            phongCB.View = viewMatrix;
            phongCB.World = XMMatrixTranspose(planeTransform.generateModelMatrix());
            phongCB.LightView = XMMatrixTranspose(lightView);
            phongCB.LightProjection = XMMatrixTranspose(lightProjection);
            phongCB.SunLight.Color = SUN_YELLOW;
            phongCB.SunLight.Direction = XMFLOAT4(-sunPos.x, -sunPos.y, -sunPos.z, 1.0f);
            phongCB.NormalMatrix = computeNormalMatrix(phongCB.World);

            // todo: textures on the ground do not work
            diffuseSampler_->use(context_.immediateContext_, 0);
            shadowSampler_->use(context_.immediateContext_, 1);
            context_.immediateContext_->PSSetShaderResources(1, 1, &shadowShaderResourceView_);
            seaFloorTexture_->use(context_.immediateContext_, 0);
            texturedPhong_->use(context_.immediateContext_);
            texturedPhong_->updateConstantBuffer(context_.immediateContext_, phongCB);
            planeModel_->draw(context_.immediateContext_);
        }


        if (renderCube) {
            SolidCB scb;
            scb.Projection = projectionMatrix;
            scb.View = viewMatrix;
            scb.World = XMMatrixTranspose(coloredCubeTransform.generateModelMatrix());
            scb.vOutputColor = DirectX::XMFLOAT4(0.3764706f, 0.2480821f, 0.06274506f, 1);

            solidShader_->updateConstantBuffer(context_.immediateContext_, scb);
            solidShader_->use(context_.immediateContext_);
            cubeModel_->draw(context_.immediateContext_);

            scb.World = XMMatrixTranspose(sunCubeTransform.generateModelMatrix());
            solidShader_->updateConstantBuffer(context_.immediateContext_, scb);
            solidShader_->use(context_.immediateContext_);
            cubeModel_->draw(context_.immediateContext_);
        }

        // Normals for debugging grass tangent space
        /*ConstantBuffer normals_cb;
        normals_cb.View = XMMatrixTranspose(camera_.getViewMatrix());
        normals_cb.Projection = XMMatrixTranspose(projection_);
        normals_cb.World = XMMatrixTranspose(modelTransform.generateModelMatrix());
        normals_cb.NormalMatrix = computeNormalMatrix(normals_cb.World);
        normalShader_->updateConstantBuffer(context_.immediateContext_, normals_cb);
        normalShader_->use(context_.immediateContext_);
        model_->draw(context_.immediateContext_);*/

        // todo: put to a separate method call
        // Render the grass:
        if (renderPlane) {
            // Draw the grass in the correct space
            planeGrassCB.worldMatrix = XMMatrixTranspose(planeTransform.generateModelMatrix());
            planeGrassCB.inverseWorldMatrix = XMMatrixInverse(nullptr, planeGrassCB.worldMatrix);
            planeGrassCB.normalMatrix = computeNormalMatrix(planeGrassCB.worldMatrix);
            planeGrassCB.projectionMatrix = projectionMatrix;
            planeGrassCB.viewMatrix = viewMatrix;
            planeGrassCB.cameraPosition = camera_.Position;

            // Draw the grass
            diffuseSampler_->use(context_.immediateContext_, 0);
            windTexture_->use(context_.immediateContext_, 0);
            diffuseSampler_->useGS(context_.immediateContext_, 0);
            windTexture_->useGS(context_.immediateContext_, 0);
            context_.immediateContext_->PSSetShaderResources(1, 1, &shadowShaderResourceView_);
            shadowSampler_->use(context_.immediateContext_, 1);
            grassShader_->updateConstantBuffer(context_.immediateContext_, planeGrassCB);
            grassShader_->use(context_.immediateContext_);
            planeModel_->draw(context_.immediateContext_, D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
        }

        if (renderPlanet) {
            // Draw the grass in the correct space
            planetGrassCB.worldMatrix = XMMatrixTranspose(planetTransform.generateModelMatrix());
            planetGrassCB.inverseWorldMatrix = XMMatrixInverse(nullptr, planetGrassCB.worldMatrix);
            planetGrassCB.normalMatrix = computeNormalMatrix(planetGrassCB.worldMatrix);
            planetGrassCB.projectionMatrix = projectionMatrix;
            planetGrassCB.viewMatrix = viewMatrix;
            planetGrassCB.cameraPosition = camera_.Position;

            // Draw the grass
            diffuseSampler_->use(context_.immediateContext_, 0);
            windTexture_->use(context_.immediateContext_, 0);
            diffuseSampler_->useGS(context_.immediateContext_, 0);
            windTexture_->useGS(context_.immediateContext_, 0);
            context_.immediateContext_->PSSetShaderResources(1, 1, &shadowShaderResourceView_);
            shadowSampler_->use(context_.immediateContext_, 1);
            grassShader_->updateConstantBuffer(context_.immediateContext_, planetGrassCB);
            grassShader_->use(context_.immediateContext_);
            planetModel_->draw(context_.immediateContext_, D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
        }

        // todo: multiple settings for bot plane and planet

        renderImGUI();
        
        if (renderPlane) {
            ImGui::Begin("Plane grass");
            if (renderModelImGUI(planeSettingsWrapper)) {
                planeSettingsWrapper->updateGrassCB(planeGrassCB, context_);
            }
        }
    
        if (renderPlanet) {
            ImGui::Begin("Planet grass");
            if (renderModelImGUI(planetSettingsWrapper)) {
                planetSettingsWrapper->updateGrassCB(planetGrassCB, context_);
            }
        }       

        ImGui::Render();
        context_.immediateContext_->OMSetRenderTargets(1, &context_.renderTargetView_, NULL);
        ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
    }   

    // Next frame texture bound as PS resource will be used as DSV, here we unbind manually to state the intent
    // Otherwise, the driver would unbind the texture forecfully, see ID3D11DeviceContext::OMSetRenderTargets on MSDN
    static ID3D11ShaderResourceView* nullViews[] = {
        nullptr, nullptr
    };
    context_.immediateContext_->PSSetShaderResources(0, 2, nullViews);
    context_.swapChain_->Present(0, 0);
}

    bool GrassRenderer::renderModelImGUI(std::unique_ptr<GrassSettingsWrapper> & settingsWrapper) {
        bool changed = false;

        ImGui::Text("Template selector:");
        {
            const char* items[] = { "default", "long grass"};
            static const char* current_item = "default";

            if (ImGui::BeginCombo("##templates combo", current_item, ImGuiComboFlags_NoArrowButton))
            {
                for (int n = 0; n < IM_ARRAYSIZE(items); n++)
                {
                    bool is_selected = (current_item == items[n]);
                    if (ImGui::Selectable(items[n], is_selected)) {
                        current_item = items[n];
                        if (selected_template != current_item) {
                            selected_template = current_item;
                            getSettingsPreset(selected_template, settingsWrapper);   
                            changed = true;
                        }
                    }

                    if (is_selected) {
                        ImGui::SetItemDefaultFocus();
                    }
                }
                ImGui::EndCombo();
            }
        }

        ImGui::Columns(3, "##1-content", false);
        if (ImGui::Button("General")) {
            current_content = 1;
        }

        ImGui::NextColumn();
        if (ImGui::Button("Tessellation")) {
            current_content = 2;
        }

        ImGui::NextColumn();
        if (ImGui::Button("Wind")) {
            current_content = 3;
        }

        ImGui::Columns(1);

        ImGui::Separator();
        if (current_content == 2) {
            const char* items[] = { "0: Distance from camera", "1: Sphere diameter","2: Uniform", "3: RandomUniform" };
            static const char* current_item = "2: Uniform";

            if (ImGui::BeginCombo("##custom combo", current_item, ImGuiComboFlags_NoArrowButton))
            {
                for (int n = 0; n < IM_ARRAYSIZE(items); n++)
                {
                    bool is_selected = (current_item == items[n]);
                    if (ImGui::Selectable(items[n], is_selected)) {
                        current_item = items[n];
                        if (settingsWrapper->lodding_method != n) {
                            changed = true;
                            
                        }

                        settingsWrapper->lodding_method = n;
                    }

                    if (is_selected) {
                        ImGui::SetItemDefaultFocus();
                    }
                }
                ImGui::EndCombo();
            }
            ImGui::SameLine();
            HelpMarker("select a tessellation lodding mode");

            changed |= ImGui::Checkbox("Backface culling", &settingsWrapper->backface_culling);
            ImGui::SameLine();
            HelpMarker("do not tessellate vertices that are not visible");

            ImGui::Text("Sphere diameter:");
            ImGui::SameLine();
            HelpMarker("how many pixels should one tessellation edge take");
            changed |= ImGui::InputFloat("Pixels per edge", &settingsWrapper->pixels_per_edge);

            ImGui::Text("Distance from camera");
            ImGui::SameLine();
            HelpMarker("ff > min distance, then we decrease the tessellation factor up to max distance, when the factor is 0");
            changed |= ImGui::InputFloat("Min distance", &settingsWrapper->min_tesselation_distance);
            changed |= ImGui::InputFloat("Max distance", &settingsWrapper->max_tesselation_distance);
            changed |= ImGui::InputInt("Max factor", &settingsWrapper->max_tesselation_factor);

            ImGui::Text("Uniform tessellation:");
            ImGui::SameLine();
            HelpMarker("tessellation factor will be the same for every vertex");
            changed |= ImGui::InputFloat("Tesselation", &settingsWrapper->tessellation_uniform);

        }
        else if (current_content == 3) {
            changed |= ImGui::SliderFloat("strength", &settingsWrapper->wind_strength, 0.001f, 1.0f);
            ImGui::SameLine();
            HelpMarker("strength of the wind");

            changed |= ImGui::InputFloat2("Tiling scale", (float*)&settingsWrapper->tiling_offset);
            ImGui::SameLine();
            HelpMarker("how do we scale the input position when sampling");

            changed |= ImGui::InputFloat2("frequency", (float*)&settingsWrapper->wind_frequency);
            ImGui::SameLine();
            HelpMarker("frequency of time when sampling");
        }
        else {
            ImGui::Text("Lightning:");
            changed |= ImGui::ColorEdit3("Dynamic light color", (float*)&settingsWrapper->dynamic_light_color);
            changed |= ImGui::InputFloat("Dynamic light strength", &settingsWrapper->dynamic_light_strenght);
            changed |= ImGui::ColorEdit3("Ambient light color", (float*)&settingsWrapper->ambient_light_color);
            changed |= ImGui::InputFloat("Ambient light strength", &settingsWrapper->ambient_light_strength);

            ImGui::Text("Grass colours:");
            changed |= ImGui::ColorEdit3("Top color", (float*)&settingsWrapper->top_color);
            changed |= ImGui::ColorEdit3("Bottom color", (float*)&settingsWrapper->bottom_color);

            ImGui::Text("Grass look:");
            changed |= ImGui::SliderFloat("blade width", &settingsWrapper->blade_width, 0.01f, 2.0f);
            ImGui::SameLine();
            HelpMarker("width of the blade");

            changed |= ImGui::SliderFloat("blade width random", &settingsWrapper->blade_width_random, 0.0f, 0.5f);
            ImGui::SameLine();
            HelpMarker("max random offset (as [-1, 1] random * blade width random)");

            changed |= ImGui::SliderFloat("blade height", &settingsWrapper->blade_height, 0.01f, 2.0f);
            ImGui::SameLine();
            HelpMarker("height of the blade");

            changed |= ImGui::SliderFloat("blade height random", &settingsWrapper->blade_height_random, 0.0f, 0.5f);
            ImGui::SameLine();
            HelpMarker("width of the blade");

            changed |= ImGui::SliderFloat("bend rotation random", &settingsWrapper->bend_rotation_random, 0.0f, 0.5f);
            ImGui::SameLine();
            HelpMarker("grass vector is bent by a slight angle");

            changed |= ImGui::SliderFloat("blade forward", &settingsWrapper->blade_forward, 0.0f, 0.5f);
            ImGui::SameLine();
            HelpMarker("random forward of the grass to where the curve will go");

            changed |= ImGui::SliderFloat("blade curve", &settingsWrapper->blade_curve, 0.1f, 5.0f);
            ImGui::SameLine();
            HelpMarker("shapes the blade curve");

            ImGui::Text("Lodding:");
            changed |= ImGui::Checkbox("Blade lodding", &settingsWrapper->blades_lodding);
            changed |= ImGui::InputFloat("Min distance", &settingsWrapper->min_lodding_distance);
            changed |= ImGui::InputFloat("Max distance", &settingsWrapper->max_lodding_distance);
        }

        ImGui::End();
        return changed;
    }

    bool GrassRenderer::renderImGUI() {
        bool changed = false;
        ImGui::Begin("General");
        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

        ImGui::Checkbox("Shadows", &shadows);
        ImGui::SameLine();
        HelpMarker("enables or disables the shadow map");

        ImGui::Text("Enable or disable rendered objects.");
        changed |= ImGui::Checkbox("Render planet", &renderPlanet);
        changed |= ImGui::Checkbox("Render plane", &renderPlane);
        changed |= ImGui::Checkbox("Render cube", &renderCube);

        ImGui::SameLine();
        ImGui::End();

        return changed;
    }


    ContextSettings GrassRenderer::getSettings() const {
        return ContextSettings{};
    }

    Mouse::Mode GrassRenderer::getInitialMouseMode() {
        return Mouse::MODE_RELATIVE;
    }

    void GrassRenderer::getSettingsPreset(std::string name, std::unique_ptr<GrassSettingsWrapper>& settingsWrapper) {
        settingsWrapper->setDefaults();
        
        if (name == "long grass") {
            settingsWrapper->blade_height = 2.0f;
            settingsWrapper->top_color = ImVec4(0.8588f,0.8156f,0.1411f, 1);
            settingsWrapper->bottom_color = ImVec4(0.0549f, 0.0705f, 0.0078f, 1);
            settingsWrapper->wind_strength = 1.0f;
            settingsWrapper->tiling_offset = ImVec2(0.001f, 0.001f);
            settingsWrapper->wind_frequency = ImVec2(0.005f, 0.005f);
            settingsWrapper->dynamic_light_strenght = 0;
            settingsWrapper->ambient_light_strength = 0.8f;
        }        
    }
}

