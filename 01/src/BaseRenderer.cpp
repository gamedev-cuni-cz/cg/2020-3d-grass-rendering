#include "BaseRenderer.h"
#include <chrono>
#include <directxcolors.h>
#include "WinKeyMap.h"

using namespace DirectX;

XMVECTORF32 Util::srgbToLinear(const XMVECTORF32& color) {
    XMVECTORF32 clearColor;
    clearColor.v = XMColorSRGBToRGB(color);
    return clearColor;
}

DirectX::XMFLOAT4 Util::srgbToLinearVec(const DirectX::XMVECTORF32& color) {
    XMVECTORF32 clearColor;
    clearColor.v = XMColorSRGBToRGB(color);
    XMFLOAT4 vec(clearColor.f[0], clearColor.f[1], clearColor.f[2], clearColor.f[3]);
    return vec;
}


HRESULT BaseRenderer::reloadShaders() {
    if (reloadShadersInternal())
        return S_OK;
    return E_FAIL;
}

void BaseRenderer::handleInput() {
    if (GetActiveWindow() != context_.hWnd_)
        return;  

    if (GetAsyncKeyState(VK_ESCAPE)) {
        shouldExit_ = true;
    }
    if (GetAsyncKeyState(WinKeyMap::W)) {
        camera_.ProcessKeyboard(CameraMovement::FORWARD, deltaTime_);
    }
    if (GetAsyncKeyState(WinKeyMap::S)) {
        camera_.ProcessKeyboard(CameraMovement::BACKWARD, deltaTime_);
    }
    if (GetAsyncKeyState(WinKeyMap::A)) {
        camera_.ProcessKeyboard(CameraMovement::LEFT, deltaTime_);
    }
    if (GetAsyncKeyState(WinKeyMap::D)) {
        camera_.ProcessKeyboard(CameraMovement::RIGHT, deltaTime_);
    }
    if (GetAsyncKeyState(VK_SPACE)) {
        camera_.ProcessKeyboard(CameraMovement::UP, deltaTime_);
    }
    if (GetAsyncKeyState(VK_CONTROL)) {
        camera_.ProcessKeyboard(CameraMovement::DOWN, deltaTime_);
    }
        

    auto mouseState = mouse_->GetState();
    if (GetAsyncKeyState(WinKeyMap::M) & 1) {
        if (mouseState.positionMode == Mouse::MODE_RELATIVE) {
            mouse_->SetMode(Mouse::MODE_ABSOLUTE);
        } else {
            mouse_->SetMode(Mouse::MODE_RELATIVE);
        }
    }

    if (mouseState.positionMode == Mouse::MODE_RELATIVE) {
        camera_.ProcessMouseMovement(static_cast<float>(-mouseState.x), static_cast<float>(mouseState.y));
    }

    if (GetAsyncKeyState(WinKeyMap::F5) & 1) {
        std::cout << "+++ Reloading shaders" << std::endl;
        if (FAILED(reloadShaders()))
            std::cout << "--- Reload failed" << std::endl;
        else
            std::cout << "--- Reload successful" << std::endl;
    }
}

HRESULT BaseRenderer::setup() {
    projection_ = DirectX::XMMatrixPerspectiveFovLH(XM_PIDIV4, context_.WIDTH / static_cast<FLOAT>(context_.HEIGHT), SCREEN_NEAR, SCREEN_DEPTH);
    return S_OK;
}

std::chrono::steady_clock::time_point lastFrame = std::chrono::high_resolution_clock::now();
void BaseRenderer::render() {
    ++frameCount_;
    // Update our time
    const auto currentFrame = std::chrono::high_resolution_clock::now();
    deltaTime_ = std::chrono::duration_cast<std::chrono::nanoseconds>(currentFrame - lastFrame).count() / 1000000000.0f;
    lastFrame = currentFrame;
    timeFromStart += deltaTime_;

    // Compute frame moving average
    if (frameCount_ == smaPeriod_) {
        deltaTimeSMA_ = timeFromStart / smaPeriod_;
    } else if (frameCount_ > smaPeriod_) {
        float newSmaSum = deltaTimeSMA_ * (smaPeriod_ - 1) + deltaTime_;
        deltaTimeSMA_ = newSmaSum / smaPeriod_;
    }

    handleInput();
}

XMMATRIX BaseRenderer::computeNormalMatrix(const DirectX::XMMATRIX & model) {
    return XMMatrixTranspose(XMMatrixInverse(nullptr, model));
}

XMMATRIX BaseRenderer::computeNormalMatrix(const std::vector<DirectX::XMMATRIX>& matrices) {
    XMMATRIX multiple = DirectX::XMMatrixIdentity();
    for (const auto& matrix : matrices) {
        multiple = XMMatrixMultiply(multiple, XMMatrixTranspose(matrix));
    }
    return XMMatrixTranspose(XMMatrixInverse(nullptr, multiple));
}

void BaseRenderer::clearViews() const {
    context_.immediateContext_->ClearRenderTargetView(context_.renderTargetView_, Util::srgbToLinear(DirectX::Colors::MidnightBlue));
    context_.immediateContext_->ClearDepthStencilView(context_.depthStencilView_, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void BaseRenderer::showMouse() {
    mouse_->SetMode(Mouse::MODE_ABSOLUTE);
}

BaseRenderer::BaseRenderer() 
    : camera_(XMFLOAT3(0.0f, 0.0f, -10.0f)) {
}

int BaseRenderer::run(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow) {
    AllocConsole();
    FILE* pCin, * pCout, * pCerr;
    freopen_s(&pCin, "conin$", "r", stdin);
    freopen_s(&pCout, "conout$", "w", stdout);
    freopen_s(&pCerr, "conout$", "w", stderr);
    std::cout << "Start" << std::endl;
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    const ContextSettings settings = getSettings();

    auto hr = context_.init(hInstance, nCmdShow, settings);
    if (FAILED(hr)) {
        MessageBox(nullptr, L"Failed to init context wrapper", L"Error", MB_OK);
        return 0;
    }

    mouse_ = std::make_unique<Mouse>();
    mouse_->SetWindow(context_.hWnd_);
    mouse_->SetMode(getInitialMouseMode());


    hr = setup();
    if (FAILED(hr)) {
        std::cout << "Failed to setup the example " << hr << std::endl;
        MessageBox(nullptr, L"Failed to setup the example", L"Error", MB_OK);
        return 0;
    }


    // Main message loop
    MSG msg = { 0 };
    while (WM_QUIT != msg.message && !shouldExit_) {
        if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE | PM_NOYIELD)) {
            switch (msg.message) {
            case WM_INPUT:
            case WM_MOUSEMOVE:
            case WM_LBUTTONDOWN:
            case WM_LBUTTONUP:
            case WM_RBUTTONDOWN:
            case WM_RBUTTONUP:
            case WM_MBUTTONDOWN:
            case WM_MBUTTONUP:
            case WM_MOUSEWHEEL:
            case WM_XBUTTONDOWN:
            case WM_XBUTTONUP:
            case WM_MOUSEHOVER:
            case WM_ACTIVATEAPP:
                Mouse::ProcessMessage(msg.message, msg.wParam, msg.lParam);
                break;
            default:
                break;
            }

            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else {
            render();
        }
    }

    return static_cast<int>(msg.wParam);
}
