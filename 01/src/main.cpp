#include "GrassRenderer.h"
#include "ShadowsExample.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>


int WINAPI wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow) { 
    // Initialize the grass renderer
    std::unique_ptr<GrassRendering::GrassRenderer> grassRenderer = std::make_unique<GrassRendering::GrassRenderer>();

    //std::unique_ptr<Shadows::ShadowsExample> shadowsExample = std::make_unique<Shadows::ShadowsExample>();
         
    return grassRenderer->run(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
}
