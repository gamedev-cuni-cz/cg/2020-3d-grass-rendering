Texture2D WindTexture : register(t0);

SamplerState WindSampler : register(s0);

// todo: Update grass shadows when the normal grass is done

struct DirLight {
    float4 Direction;
    float4 Color;
};

cbuffer ConstantBuffer : register(b0) {
    matrix World; // todo: rename all matrices to have Matrix at the end
    matrix InverseWorld;
    matrix View;
    matrix Projection;
    matrix NormalMatrix;
    matrix LightView;
    matrix LightProjection;

    float4 topColor;
    float4 bottomColor;
    float4 tilingOffset;
    float4 dynamicLightDirection;
    float dynamicLightStrength;
    float3 dynamicLightColor;
    float2 windFrequency;
    float bendRotationRandom;
    float bladeWidth;
    float3 cameraPosition;
    float translucentGain;
    float3 dynamicLightPosition;
    float ambientLightStrength;
    float3 ambientLightColor;


    float bladeWidthRandom;
    float bladeHeight;
    float bladeHeightRandom;

    float tessellationUniform;

    float MinTessellationDistance;
    float MaxTessellationDistance;

    float edgesPerScreenHeight;
    float windStrength;
    float bladeForward;
    float bladeCurve;
    float time;


    int MaxTessellationFactor;

    int loddingMethod;
    bool backfaceCulling;
}

struct vertexInput
{
    float4 vertex : POSITION;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
};

struct vertexOutput
{
    float4 pos : SV_POSITION;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
};

struct TessellationFactors {
    float edge[3] : SV_TessFactor;
    float inside : SV_InsideTessFactor;
};


struct geometryOutput {
    float4 pos : SV_POSITION;
    float2 uv : TEXCOORD0;
    float3 normal: NORMAL;
};

static const float PI = 3.14159265f;
static const float TWO_PI = 6.28318530718f;
static const int BLADE_SEGMENTS = 3;

// Simple noise function, sourced from http://answers.unity.com/answers/624136/view.html
    // Extended discussion on this function can be found at the following link:
    // https://forum.unity.com/threads/am-i-over-complicating-this-random-function.454887/#post-2949326
    // Returns a number in the 0...1 range.
float rand(float3 co)
{
    return frac(sin(dot(co.xyz, float3(12.9898, 78.233, 53.539))) * 43758.5453);
}

// Construct a rotation matrix that rotates around the provided axis, sourced from:
// https://gist.github.com/keijiro/ee439d5e7388f3aafc5296005c8c3f33
float3x3 AngleAxis3x3(float angle, float3 axis)
{
    float c, s;
    sincos(angle, s, c);

    float t = 1 - c;
    float x = axis.x;
    float y = axis.y;
    float z = axis.z;

    return float3x3(
        t * x * x + c, t * x * y - s * z, t * x * z + s * y,
        t * x * y + s * z, t * y * y + c, t * y * z - s * x,
        t * x * z - s * y, t * y * z + s * x, t * z * z + c
        );
}



// =============
// Vertex shader
// =============
vertexInput VS(vertexInput input) {
    //vertexOutput output = (vertexOutput) 0;
    //output.pos = input.vertex;
    //output.tangent = input.tangent;
    //output.normal = input.normal;
    //return output;
    return input;
}

geometryOutput VertexOutput(float4 pos, float2 uv, float3 normal)
{
    geometryOutput output = (geometryOutput) 0;
    output.pos = mul(pos, World);
    output.pos = mul(output.pos, View);
    output.pos = mul(output.pos, Projection);
    output.uv = uv;
    output.normal = mul(
        transpose((float3x3)InverseWorld),
        normal
    );

    return output;
}

geometryOutput GenerateGrassVertex(float4 vertexPosition, float width, float height,
    float forward, float2 uv, float3x3 transformMatrix)
{
    float3 tangentPoint = float3(width, forward, height);
    
    float3 tangentNormal = normalize(float3(0, -1, forward));
    float3 localNormal = mul(transformMatrix, tangentNormal);


    float4 localPosition = vertexPosition + float4(mul(transformMatrix, tangentPoint), 0);
    return VertexOutput(localPosition, uv, localNormal);
}

// ===============
// Geometry shader
// ===============
[maxvertexcount(BLADE_SEGMENTS * 2 + 1)]
//[maxvertexcount(3)]
void GS(triangle vertexOutput input[3], inout TriangleStream<geometryOutput> triangleStream) {     
    float3 vNormal = mul(float4(input[0].normal, 1), NormalMatrix).xyz;
    float3 vTangent = normalize(mul(NormalMatrix, float4(input[0].tangent, 1)).xyz);
    vTangent = normalize(vTangent - dot(vTangent, vNormal) * vNormal);
    float3 vBinormal = normalize(cross(vNormal, vTangent));

    // Matrix for transforming into tangent space
    float3x3 tangentToLocal = float3x3(
        vTangent.x, vBinormal.x, vNormal.x,
        vTangent.y, vBinormal.y, vNormal.y,
        vTangent.z, vBinormal.z, vNormal.z
        );

    // Height and width generation:
    float height = (rand(input[0].pos.zyx) * 2 - 1) * bladeHeightRandom + bladeHeight;
    float width = (rand(input[0].pos.xzy) * 2 - 1) * bladeWidthRandom + bladeWidth;
    float forward = rand(input[0].pos.yyz) * bladeForward;

    // Facing direction: 
    float3x3 facingRotationMatrix = AngleAxis3x3(rand(input[0].pos) * TWO_PI, float3(0, 0, 1));
    
    // Bend rotation matrix:
    float3x3 bendRotationMatrix = AngleAxis3x3(rand(input[0].pos.zzx) * bendRotationRandom * PI * 0.5, float3(-1, 0, 0));

    // Wind uv
    float2 uv = input[0].pos.xz * tilingOffset.xy + tilingOffset.zw + windFrequency * time;
    float2 textureSample = WindTexture.SampleLevel(WindSampler, uv * 2 - 1, 0);
    float2 windSample = textureSample * windStrength;
    float3 wind = normalize(float3(windSample.x, windSample.y, 0));
    float3x3 windRotation = AngleAxis3x3(PI * windSample, wind);

    // Combined transformation matrix:
    float3x3 transformationMatrix = mul(mul(mul(tangentToLocal, windRotation), facingRotationMatrix), bendRotationMatrix);
    
    // A matrix for repairing the base of the blade
    float3x3 transformationMatrixFacing = mul(tangentToLocal, facingRotationMatrix);

    //float3x3 transformationMatrix = mul(mul(tangentToLocal, facingRotationMatrix), bendRotationMatrix);
    
        // Generate grass segments
    for (int i = 0; i < BLADE_SEGMENTS; i++)
    {
        // How far vertically along the blade are we
        float t = i / (float)BLADE_SEGMENTS;

        float segmentHeight = height * t;
        float segmentWidth = width * (1 - t);
        float segmentForward = pow(t, bladeCurve) * forward;

        float3x3 transformMatrix = i == 0 ? transformationMatrixFacing : transformationMatrix;
        triangleStream.Append(GenerateGrassVertex(input[0].pos, segmentWidth, segmentHeight, segmentForward, float2(0, t), transformMatrix));
        triangleStream.Append(GenerateGrassVertex(input[0].pos, -segmentWidth, segmentHeight, segmentForward, float2(1, t), transformMatrix));

    }

    triangleStream.Append(GenerateGrassVertex(input[0].pos, 0, height, forward, float2(0.5, 1), transformationMatrix));
}

vertexOutput tessVert(vertexInput v)
{
    vertexOutput o = (vertexOutput) 0;   
    o.pos = v.vertex;
    o.normal = v.normal;
    o.tangent = v.tangent;
    return o;
}

[domain("tri")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("patchConstantFunction")]
vertexInput hull(InputPatch<vertexInput, 3> patch, uint id : SV_OutputControlPointID) { 
    return patch[id];
}


TessellationFactors patchConstantFunction(InputPatch<vertexInput, 3> patch)
{
    TessellationFactors f;
    f.edge[0] = tessellationUniform;
    f.edge[1] = tessellationUniform;
    f.edge[2] = tessellationUniform;
    f.inside = tessellationUniform;
    return f;
}


[domain("tri")]
vertexOutput domain(TessellationFactors factors, OutputPatch<vertexInput, 3> patch, float3 barycentricCoordinates : SV_DomainLocation)
{
    vertexInput v;

    #define MY_DOMAIN_PROGRAM_INTERPOLATE(fieldName) v.fieldName = \
					patch[0].fieldName * barycentricCoordinates.x + \
					patch[1].fieldName * barycentricCoordinates.y + \
					patch[2].fieldName * barycentricCoordinates.z;

    MY_DOMAIN_PROGRAM_INTERPOLATE(vertex)
    MY_DOMAIN_PROGRAM_INTERPOLATE(normal)
    MY_DOMAIN_PROGRAM_INTERPOLATE(tangent)

    return tessVert(v);
}


float4 EncodeCubeShadowDepth(float z)
{
    return z;

}

// ============
// Pixel shader
// ============
float4 PS(geometryOutput input, bool isFrontFacing : SV_IsFrontFace) : SV_Target{
    return float4(0,0,0,1);
}
