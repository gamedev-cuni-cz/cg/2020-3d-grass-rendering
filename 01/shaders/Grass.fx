Texture2D WindTexture : register(t0);
Texture2D ShadowMap : register(t1);

SamplerState WindSampler : register(s0);
SamplerState samPoint : register(s1);

// =============
// Constant buffer
// =============
cbuffer ConstantBuffer : register(b0) {
    matrix worldMatrix;
    matrix inverseWorldMatrix;
    matrix viewMatrix;
    matrix projectionMatrix;
    matrix normalMatrix;
    matrix lightViewMatrix;
    matrix lightProjectionMatrix;
    
    float4 topColor; 
    float4 bottomColor;
    float4 tilingOffset;
    float4 dynamicLightDirection;
    float dynamicLightStrength;
    float3 dynamicLightColor;    
    float2 windFrequency;
    float bendRotationRandom;
    float bladeWidth;
    float3 cameraPosition; 
    float translucentGain;
    float3 dynamicLightPosition;
    float ambientLightStrength;
    float3 ambientLightColor;
        
    float bladeWidthRandom;
    float bladeHeight;
    float bladeHeightRandom;
    
    float tessellationUniform;
    
    float minTessellationDistance;
    float maxTessellationDistance;
    
    float edgesPerScreenHeight;
    float windStrength;
    float bladeForward;
    float bladeCurve;
    float time;        

    int maxTessellationFactor;

    int loddingMethod;
    bool backfaceCulling;
    bool bladesLodding;
    float minLoddingDistance;
    float maxLoddingDistance;
}

struct vertexInput
{
    float4 vertex : POSITION;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
};

struct vertexOutput
{
    float4 pos : SV_POSITION;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
};

struct TessellationFactors {
    float edge[3] : SV_TessFactor;
    float inside : SV_InsideTessFactor;
};


struct geometryOutput {
    float4 pos : SV_POSITION;
    float2 uv : TEXCOORD0;
    float3 normal: NORMAL;  
    float3 fragPos : FRAG_POSITION;
    float4 fragPosLightSpace : FRAG_LIGHT_POSITION;
};

static const float PI = 3.14159265f;
static const float TWO_PI = 6.28318530718f;
static const int BLADE_SEGMENTS = 3;

// Simple noise function, sourced from http://answers.unity.com/answers/624136/view.html
// Extended discussion on this function can be found at the following link:
// https://forum.unity.com/threads/am-i-over-complicating-this-random-function.454887/#post-2949326
// Returns a number in the 0...1 range.
float rand(float3 co)
{
    return frac(sin(dot(co.xyz, float3(12.9898, 78.233, 53.539))) * 43758.5453);
}

// Construct a rotation matrix that rotates around the provided axis, sourced from:
// https://gist.github.com/keijiro/ee439d5e7388f3aafc5296005c8c3f33
float3x3 AngleAxis3x3(float angle, float3 axis)
{
    float c, s;
    sincos(angle, s, c);

    float t = 1 - c;
    float x = axis.x;
    float y = axis.y;
    float z = axis.z;

    return float3x3(
        t * x * x + c, t * x * y - s * z, t * x * z + s * y,
        t * x * y + s * z, t * y * y + c, t * y * z - s * x,
        t * x * z - s * y, t * y * z + s * x, t * z * z + c
        );
}

// =============
// Vertex shader
// =============
vertexInput VS(vertexInput input) {
    return input;
}

geometryOutput VertexOutput(float4 pos, float2 uv, float3 normal)
{
    geometryOutput output = (geometryOutput) 0;
    output.pos = mul(pos, worldMatrix);
    output.pos = mul(output.pos, viewMatrix);
    output.pos = mul(output.pos, projectionMatrix);
    output.uv = uv;
    output.normal = mul(
        transpose((float3x3)inverseWorldMatrix),
        normal
    );

    output.fragPos = mul(pos, worldMatrix);
    output.fragPosLightSpace = mul(float4(output.fragPos, 1.0), lightViewMatrix);
    output.fragPosLightSpace = mul(output.fragPosLightSpace, lightProjectionMatrix);
    return output;
}

geometryOutput GenerateGrassVertex(float4 vertexPosition, float width, float height,
    float forward, float2 uv, float3x3 transformMatrix)
{
    float3 tangentPoint = float3(width, forward, height);
    
    float3 tangentNormal = normalize(float3(0, -1, forward));
    float3 localNormal = mul(transformMatrix, tangentNormal);

    float4 localPosition = vertexPosition + float4(mul(transformMatrix, tangentPoint), 0);
    return VertexOutput(localPosition, uv, localNormal);
}

// ===============
// Geometry shader
// ===============
[maxvertexcount(BLADE_SEGMENTS * 2 + 1)]
//[maxvertexcount(3)]
void GS(triangle vertexOutput input[3], inout TriangleStream<geometryOutput> triangleStream) {     
    float3 vNormal = mul(float4(input[0].normal, 1), normalMatrix).xyz;
    float3 vTangent = normalize(mul(normalMatrix, float4(input[0].tangent, 1)).xyz);
    vTangent = normalize(vTangent - dot(vTangent, vNormal) * vNormal);
    float3 vBinormal = normalize(cross(vNormal, vTangent));

    // Matrix for transforming into tangent space
    float3x3 tangentToLocal = float3x3(
        vTangent.x, vBinormal.x, vNormal.x,
        vTangent.y, vBinormal.y, vNormal.y,
        vTangent.z, vBinormal.z, vNormal.z
        );

    ///////////
    // Facing direction: 
    float3x3 facingRotationMatrix = AngleAxis3x3(rand(input[0].pos) * TWO_PI, float3(0, 0, 1));
    
    // Bend rotation matrix:
    // We bend the grass in a random angle
    float3x3 bendRotationMatrix = AngleAxis3x3(rand(input[0].pos.zzx) * bendRotationRandom * PI * 0.5, float3(-1, 0, 0));
    ///////////

    ///////////
    // Wind: 
    // We sample each grass vertex using its xz coordinates, we move the wind texture, 
    // so that it all looks as part of the same system.

    // Calculate the uv: 
    // Scale the position by a tiling offset, so that we can define the texture precision
    // Move the uv over time by given frequency
    float2 uv = input[0].pos.xz * tilingOffset.xy + tilingOffset.zw + windFrequency * time;

    // Rescale from [0, 1] range to [-1, 1] range
    float2 textureSample = WindTexture.SampleLevel(WindSampler, uv * 2 - 1, 0); 
    
    // Apply wind strength
    float2 windSample = textureSample * windStrength;
    
    // Normalize -> direction of the wind
    float3 wind = normalize(float3(windSample.x, windSample.y, 0));

    // Create a matrix to rotate the grass by wind direction
    float3x3 windRotation = AngleAxis3x3(PI * windSample, wind);
    ///////////

    // Combined transformation matrix:
    float3x3 transformationMatrix = mul(mul(mul(tangentToLocal, windRotation), facingRotationMatrix), bendRotationMatrix);
    
    // A matrix for repairing the base of the blade
    float3x3 transformationMatrixFacing = mul(tangentToLocal, facingRotationMatrix);

    ///////////
    // Height, width, forward bend:
    // We take height/width and offset it by some random amount
    float height = (rand(input[0].pos.zyx) * 2 - 1) * bladeHeightRandom + bladeHeight;
    float width = (rand(input[0].pos.xzy) * 2 - 1) * bladeWidthRandom + bladeWidth;
    float forward = rand(input[0].pos.yyz) * bladeForward;

    int bladeSegments = BLADE_SEGMENTS;
    if (bladesLodding) {
        float distanceToCamera = distance(input[0].pos.xyz, cameraPosition);
        if (distanceToCamera > minLoddingDistance) {
            float distanceFactor = min((distanceToCamera - minLoddingDistance) / maxLoddingDistance, 1);
            bladeSegments = max(1, (1 - distanceFactor) * BLADE_SEGMENTS);
        }
    }

    // Generate grass segments
    for (int i = 0; i < bladeSegments; i++)
    {
        // How far vertically along the blade are we
        float t = i / (float)BLADE_SEGMENTS;

        float segmentHeight = height * t;
        float segmentWidth = width * (1 - t);
        float segmentForward = pow(t, bladeCurve) * forward;

        float3x3 transformMatrix = i == 0 ? transformationMatrixFacing : transformationMatrix;
        triangleStream.Append(GenerateGrassVertex(input[0].pos, segmentWidth, segmentHeight, segmentForward, float2(0, t), transformMatrix));
        triangleStream.Append(GenerateGrassVertex(input[0].pos, -segmentWidth, segmentHeight, segmentForward, float2(1, t), transformMatrix));
    }

    triangleStream.Append(GenerateGrassVertex(input[0].pos, 0, height, forward, float2(0.5, 1), transformationMatrix));
}

vertexOutput tessVert(vertexInput v)
{
    vertexOutput o = (vertexOutput) 0;   
    o.pos = v.vertex;
    o.normal = v.normal;
    o.tangent = v.tangent;
    return o;
}

[domain("tri")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("patchConstantFunction")]
vertexInput hull(InputPatch<vertexInput, 3> patch, uint id : SV_OutputControlPointID) { 
    return patch[id];
}

float GetPostProjectionSphereExtent(float3 Origin, float Diameter)
{
    float4 ClipPos = mul(viewMatrix, float4(Origin, 1.0f));
    return abs(Diameter * worldMatrix[1][1] / ClipPos.w);
}

float CalculateTessellationFactor(float4 Control0, float4 Control1)
{
    float e0 = distance(Control0, Control1);
    float3 m0 = (Control0 + Control1) / 2;
    return max(1, edgesPerScreenHeight * GetPostProjectionSphereExtent(m0, e0));
}

TessellationFactors patchConstantFunction(InputPatch<vertexInput, 3> patch) {
    // Backface culling - do not tessellate vertices I am not looking at
    if (backfaceCulling) {
        float3 edge0 = patch[1].vertex.xyz - patch[0].vertex.xyz;
        float3 edge2 = patch[2].vertex.xyz - patch[0].vertex.xyz;

        float3 faceNormal = normalize(cross(edge2, edge0));
        float3 viewVector = normalize(patch[0].vertex.xyz - cameraPosition);

        // Dot product is negative when facing off the camera
        if (dot(viewVector, faceNormal) < -0.25f) {
            TessellationFactors f;
            f.edge[0] = 0;
            f.edge[1] = 0;
            f.edge[2] = 0;
            f.inside = 0;
            return f;
        }
    }

    // The �sphere diameter in clip space� heuristic
    // https://developer.nvidia.com/content/dynamic-hardware-tessellation-basics
    if (loddingMethod == 1) {
        float tessellationMax = -100.0f;
        TessellationFactors f;
        for (int i = 0; i < 3; i++) {
            float factor = CalculateTessellationFactor(patch[i].vertex, patch[(i + 1) % 3].vertex);
            f.edge[i] = factor;
            tessellationMax = max(tessellationMax, factor);
        }

        f.inside = tessellationMax;
        return f;
    }
    // Uniform tessellation also in case when we do lodding on the grass blades
    else if (loddingMethod == 2) {
        TessellationFactors f;
        f.edge[0] = tessellationUniform;
        f.edge[1] = tessellationUniform;
        f.edge[2] = tessellationUniform;
        f.inside = tessellationUniform;
        return f;
    }
    else if (loddingMethod == 3) {
        float randomOffset = rand(patch[0].vertex.xyz);
        TessellationFactors f;
        f.edge[0] = tessellationUniform + randomOffset;
        f.edge[1] = tessellationUniform + randomOffset;
        f.edge[2] = tessellationUniform + randomOffset;
        f.inside = tessellationUniform + randomOffset;
        return f;
    }

    // LOD by the distance from the camera
    float3 objectCenter = (patch[0].vertex.xyz + patch[1].vertex.xyz + patch[2].vertex.xyz) / 3.0f;
    float3 worldCenter = mul(float4(objectCenter, 1.0f), worldMatrix).xyz;

    float distanceToCamera = distance(worldCenter, cameraPosition);

    float tesselationFactor = maxTessellationFactor;
    if (distanceToCamera > minTessellationDistance) {
        float distanceFactor = min((distanceToCamera - minTessellationDistance) / maxTessellationDistance, 1);
        tesselationFactor = (1 - distanceFactor) * maxTessellationFactor;
    }

    TessellationFactors f;
    f.edge[0] = tesselationFactor;
    f.edge[1] = tesselationFactor;
    f.edge[2] = tesselationFactor;
    f.inside = tesselationFactor;
    
    return f;
}


[domain("tri")]
vertexOutput domain(TessellationFactors factors, OutputPatch<vertexInput, 3> patch, float3 barycentricCoordinates : SV_DomainLocation)
{
    vertexInput v;

    #define MY_DOMAIN_PROGRAM_INTERPOLATE(fieldName) v.fieldName = \
					patch[0].fieldName * barycentricCoordinates.x + \
					patch[1].fieldName * barycentricCoordinates.y + \
					patch[2].fieldName * barycentricCoordinates.z;

    MY_DOMAIN_PROGRAM_INTERPOLATE(vertex)
    MY_DOMAIN_PROGRAM_INTERPOLATE(normal)
    MY_DOMAIN_PROGRAM_INTERPOLATE(tangent)

    return tessVert(v);
}

float ShadowCalculation(float4 fragPosLightSpace)
{
    // perform perspective divide
    float3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;

    // transform to [0,1] range
    projCoords.x = projCoords.x * 0.5 + 0.5;
    projCoords.y = projCoords.y * (-0.5) + 0.5;

    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = ShadowMap.Sample(samPoint, projCoords.xy).r.r;

    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;

    float bias = 0.005;
    
    // check whether current frag pos is in shadow
    float shadow = currentDepth - bias > closestDepth ? 1.0 : 0.0;

    return shadow;
}

// ============
// Pixel shader
// ============
float4 PS(geometryOutput input, bool isFrontFacing : SV_IsFrontFace) : SV_Target{
    //return lerp(bottomColor, topColor, input.uv.y);

    /*float3 normal = isFrontFacing > 0 ? input.normal : -input.normal;
    return float4(normal * 0.5 + 0.5, 1);*/

    float3 sampleColor = lerp(bottomColor, topColor, input.uv.y);
    float3 ambientLight = ambientLightColor * ambientLightStrength;
    float3 vectorToLight = normalize(dynamicLightPosition - input.fragPosLightSpace);

    float3 normal = isFrontFacing > 0 ? input.normal : -input.normal;
    //float3 diffuseLightIntensity = max(dot(vectorToLight, input.normal), 0);
    float3 diffuseLightIntensity = max(dot(vectorToLight, normal), 0);
    float3 diffuseLight = diffuseLightIntensity * dynamicLightStrength * dynamicLightColor;

    float shadow = ShadowCalculation(input.fragPosLightSpace);
    float3 finalColor = (ambientLight + (1.0 - shadow) * diffuseLight) * sampleColor;

    return float4(finalColor, 1.0f);
}
