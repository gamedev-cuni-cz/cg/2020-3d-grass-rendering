cbuffer ConstantBuffer : register(b0) {
    matrix World;
    matrix View;
    matrix Projection;
    matrix NormalMatrix;
}

struct vertexInput
{
    float4 vertex : POSITION;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
};

struct GSPS_INPUT {
    float4 pos : SV_POSITION;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
};

struct PX_INPUT {
    float4 pos : SV_POSITION;
    float2 uv : TEXCOORD0;
};

// =============
// Vertex shader
// =============
GSPS_INPUT VS(vertexInput input) {
    GSPS_INPUT output = (GSPS_INPUT)0;
    
    output.pos = input.vertex;
    //output.pos = mul(input.vertex, World);
    //output.pos = mul(output.pos, View);
    //output.pos = mul(output.pos, Projection);

    output.tangent = input.tangent;
    output.normal = input.normal;

    //float MAGNITUDE = 1.0f;
    //output.normal = float4(vNormal, 0) * MAGNITUDE; //input.vertex + float4(vNormal, 0) * MAGNITUDE;
    //output.normal = mul(output.normal, World);
    //output.normal = mul(output.normal, View);
    //output.normal = mul(output.normal, Projection);

    //output.tangent = float4(vTangent, 0);// * MAGNITUDE; // input.vertex + float4(vTangent, 0) * MAGNITUDE;
    //output.tangent = mul(output.tangent, World);
   // output.tangent = mul(output.tangent, View);
    //output.tangent = mul(output.tangent, Projection);

    //output.binormal = float4(vBinormal, 0);// * MAGNITUDE; //input.vertex + float4(vBinormal, 0) * MAGNITUDE;
   //output.binormal = mul(output.binormal, World);
    //output.binormal = mul(output.binormal, View);
    //output.binormal = mul(output.binormal, Projection);

    return output;
}

// ===============
// Geometry shader
// ===============
[maxvertexcount(21)]
void GS(triangle GSPS_INPUT input[3], inout LineStream<PX_INPUT> lineStream) {
    float3 vNormal = mul(float4(input[0].normal, 1), NormalMatrix).xyz;
    float3 vTangent = normalize(mul(NormalMatrix, float4(input[0].tangent, 1)).xyz);
    vTangent = normalize(vTangent - dot(vTangent, vNormal) * vNormal);
    float3 vBinormal = normalize(cross(vNormal, vTangent));

    // Matrix for transforming into tangent space
    float3x3 tangentToLocal = float3x3(
        vTangent.x, vBinormal.x, vNormal.x,
        vTangent.y, vBinormal.y, vNormal.y,
        vTangent.z, vBinormal.z, vNormal.z
        );
    
    PX_INPUT output = (PX_INPUT)0;
    
    output.uv = float2(1, 1);
    
    // Output the "grass"
    output.pos = input[0].pos + float4(mul(tangentToLocal, float3(-0.5f, 0, 0)), 0);
    output.pos = mul(output.pos, World);
    output.pos = mul(output.pos, View);
    output.pos = mul(output.pos, Projection);
    lineStream.Append(output);

    output.pos = input[0].pos + float4(mul(tangentToLocal, float3(0.5f, 0, 0)), 0);
    output.pos = mul(output.pos, World);
    output.pos = mul(output.pos, View);
    output.pos = mul(output.pos, Projection);
    lineStream.Append(output);

    output.pos = input[0].pos + float4(mul(tangentToLocal, float3(0, 0, 1.0f)), 0);
    output.pos = mul(output.pos, World);
    output.pos = mul(output.pos, View);
    output.pos = mul(output.pos, Projection);
    lineStream.Append(output);

    lineStream.RestartStrip();

    // Output the debug 
    for (int i = 0; i < 1; ++i) {
        output.uv = float2(1, 0);
        output.pos = mul(input[i].pos, World);
        output.pos = mul(output.pos, View);
        output.pos = mul(output.pos, Projection);
        
        lineStream.Append(output);
        
        output.pos = input[i].pos + float4(vNormal, 0);
        output.pos = mul(output.pos, World);
        output.pos = mul(output.pos, View);
        output.pos = mul(output.pos, Projection);
        lineStream.Append(output);
        
        lineStream.RestartStrip();

        output.uv = float2(0, 1);
        output.pos = input[i].pos;
        output.pos = mul(output.pos, World);
        output.pos = mul(output.pos, View);
        output.pos = mul(output.pos, Projection);
        lineStream.Append(output);

        output.pos = input[i].pos + float4(vTangent, 0);
        output.pos = mul(output.pos, World);
        output.pos = mul(output.pos, View);
        output.pos = mul(output.pos, Projection);
        lineStream.Append(output);

        lineStream.RestartStrip();

        output.uv = float2(0.5, 0.5);
        output.pos = input[i].pos;
        output.pos = mul(output.pos, World);
        output.pos = mul(output.pos, View);
        output.pos = mul(output.pos, Projection);
        lineStream.Append(output);

        output.pos = input[i].pos + float4(vBinormal, 0);
        output.pos = mul(output.pos, World);
        output.pos = mul(output.pos, View);
        output.pos = mul(output.pos, Projection);
        lineStream.Append(output);

        lineStream.RestartStrip();
    }
}

// ============
// Pixel shader
// ============
float4 PS(PX_INPUT input) : SV_Target {
    return float4(input.uv.x*1.0, input.uv.y*1.0, 0.0, 1.0);
}
